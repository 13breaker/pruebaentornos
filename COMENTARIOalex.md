**INICIALIZAMOS CON GIT**

aleeex@aleeex-VirtualBox:~/Documentos/PruebaEnt$ git init
Inicializado repositorio Git vacío en /home/aleeex/Documentos/PruebaEnt/.git/

**HACEMOS UN GIT STATUS**

aleeex@aleeex-VirtualBox:~/Documentos/PruebaEnt$ git status
En la rama master

No hay commits todavía

Archivos sin seguimiento:
  (usa "git add <archivo>..." para incluirlo a lo que se será confirmado)

	PracticaEnt

no hay nada agregado al commit pero hay archivos sin seguimiento presentes (usa "git add" para hacerles seguimiento)

**AÑADIMOS CON UN ADD**

aleeex@aleeex-VirtualBox:~/Documentos/PruebaEnt$ git add PracticaEnt 

**HACEMOS LA CONFIRMACION DEL ARCHIVO**

aleeex@aleeex-VirtualBox:~/Documentos/PruebaEnt$ git commit -m PracticaEnt 
[master (commit-raíz) 235ebd7] PracticaEnt
 1 file changed, 2 insertions(+)
 create mode 100644 PracticaEnt
aleeex@aleeex-VirtualBox:~/Documentos/PruebaEnt$ git status
En la rama master
nada para hacer commit, el árbol de trabajo está limpio

**HACEMOS UN GIT PUSH**

aleeex@aleeex-VirtualBox:~/Documentos/PruebaEnt$ git push https://13breaker@bitbucket.org/13breaker/pruebaentornos.git master
Password for 'https://13breaker@bitbucket.org': 
Enumerando objetos: 3, listo.
Contando objetos: 100% (3/3), listo.
Comprimiendo objetos: 100% (2/2), listo.
Escribiendo objetos: 100% (3/3), 272 bytes | 272.00 KiB/s, listo.
Total 3 (delta 0), reusado 0 (delta 0)
To https://bitbucket.org/13breaker/pruebaentornos.git
 * [new branch]      master -> master
 
 **POR ULTIMO UN GIT STATUS**

aleeex@aleeex-VirtualBox:~/Documentos/PruebaEnt$ git status
En la rama master
nada para hacer commit, el árbol de trabajo está limpio
